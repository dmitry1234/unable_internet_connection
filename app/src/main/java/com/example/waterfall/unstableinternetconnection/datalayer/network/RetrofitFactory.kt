package com.example.waterfall.unstableinternetconnection.datalayer.network

import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.schedulers.Schedulers

class RetrofitFactory<T> {

    companion object {
        const val BASE_URL = "https://api.unsplash.com/"
    }

    var retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(getLoggingInterceptor())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()

    private fun getLoggingInterceptor(): OkHttpClient{
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder().addInterceptor(interceptor).build()
    }

    fun createService(service: Class<T>): T {
        return retrofit.create(service)
    }

}