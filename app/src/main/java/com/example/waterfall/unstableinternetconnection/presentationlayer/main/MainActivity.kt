package com.example.waterfall.unstableinternetconnection.presentationlayer.main

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.example.waterfall.unstableinternetconnection.R
import com.example.waterfall.unstableinternetconnection.datalayer.NetworkService
import com.example.waterfall.unstableinternetconnection.datalayer.NetworkService.Companion.BROADCAST_ACTION
import com.example.waterfall.unstableinternetconnection.datalayer.NetworkService.Companion.PARAM_STATUS
import com.example.waterfall.unstableinternetconnection.datalayer.NetworkService.Companion.SERVER_STATUS_ERROR
import com.example.waterfall.unstableinternetconnection.datalayer.NetworkService.Companion.SERVER_STATUS_SUCCESS
import com.example.waterfall.unstableinternetconnection.datalayer.models.Picture
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), IMainView {

    private var presenter: IMainPresenter? = null
    private var networkBroadcastReceiver: BroadcastReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = MainPresenter()
        presenter?.onAttachView(this)

        btnTryToConnect.setOnClickListener { presenter?.onBtnTryToConnectClick() }
    }

    override fun onDestroy() {
        try {
            unregisterReceiver(networkBroadcastReceiver)
        } catch (e: IllegalArgumentException){
            e.printStackTrace()
        }
        super.onDestroy()
    }

    override fun showData(list: List<Picture>) {
        Toast.makeText(this, "The first id is ${list[0].id}", Toast.LENGTH_SHORT).show()
    }

    override fun showError() {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
    }

    override fun startNetworkService() {
        registerReceiver()
        startService(Intent(this, NetworkService::class.java))
    }

    private fun registerReceiver() {
        networkBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, p1: Intent?) {
                when (intent.getIntExtra(PARAM_STATUS, SERVER_STATUS_ERROR)) {
                    SERVER_STATUS_SUCCESS -> presenter?.onServerStatusSuccess()
                    SERVER_STATUS_ERROR -> presenter?.onServerStatusError()
                }
            }
        }
        registerReceiver(networkBroadcastReceiver, IntentFilter(BROADCAST_ACTION))
    }

}
