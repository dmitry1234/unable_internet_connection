package com.example.waterfall.unstableinternetconnection.datalayer.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
class Picture (@PrimaryKey val id: String)