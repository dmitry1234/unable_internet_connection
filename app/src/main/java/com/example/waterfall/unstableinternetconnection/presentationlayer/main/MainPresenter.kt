package com.example.waterfall.unstableinternetconnection.presentationlayer.main

import com.example.waterfall.App
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainPresenter : IMainPresenter {

    companion object {
        const val CLIENT_ID = "ff2b7fe758fb4f7369f5735abbbc9b35d7ec1e1f8edf8f7c932f003fb0191ca2"
    }

    var view: IMainView? = null
    private val pictureDao = App.instance.database?.pictureDao()


    override fun onAttachView(view: IMainView) {
        this.view = view
        pictureDao?.getStreamPictures()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    view.showData(it)
                }, {
                    view.showError()
                })

    }

    override fun onDetachView() {
        view = null
    }

    override fun onBtnTryToConnectClick() {
        view?.showProgress()
        view?.startNetworkService()
    }

    override fun onServerStatusSuccess() {
        view?.hideProgress()
    }

    override fun onServerStatusError() {
        view?.hideProgress()
    }




}