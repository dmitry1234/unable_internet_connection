package com.example.waterfall.unstableinternetconnection.datalayer.repository

import com.example.waterfall.unstableinternetconnection.datalayer.models.Picture
import com.example.waterfall.unstableinternetconnection.datalayer.network.RetrofitFactory
import com.example.waterfall.unstableinternetconnection.datalayer.services.ImagesService
import retrofit2.Call

class ImagesRepository {

    var imagesService: ImagesService = RetrofitFactory<ImagesService>().createService(ImagesService::class.java)

    fun getImages(clientId: String): Call<List<Picture>> =
            imagesService.getPictures(clientId)

}