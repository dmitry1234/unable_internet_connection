package com.example.waterfall.unstableinternetconnection.datalayer.services

import com.example.waterfall.unstableinternetconnection.datalayer.models.Picture
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ImagesService {

    @GET("photos")
    fun getPictures(@Query("client_id") clientId: String): Call<List<Picture>>

}