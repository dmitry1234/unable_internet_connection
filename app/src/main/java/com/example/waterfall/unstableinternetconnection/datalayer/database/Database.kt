package com.example.waterfall.unstableinternetconnection.datalayer.database

import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.Database
import com.example.waterfall.unstableinternetconnection.datalayer.models.Picture


@Database(entities = [Picture::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun pictureDao(): PictureDao
}