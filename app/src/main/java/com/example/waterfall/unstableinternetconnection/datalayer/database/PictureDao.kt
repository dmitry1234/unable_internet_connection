package com.example.waterfall.unstableinternetconnection.datalayer.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.waterfall.unstableinternetconnection.datalayer.models.Picture
import io.reactivex.Flowable

@Dao
interface PictureDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(list: List<Picture>)

    @Query("SELECT * FROM picture")
    fun getStreamPictures(): Flowable<List<Picture>>

}