package com.example.waterfall.unstableinternetconnection.datalayer

import android.annotation.TargetApi
import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import com.example.waterfall.App
import com.example.waterfall.unstableinternetconnection.R
import com.example.waterfall.unstableinternetconnection.datalayer.repository.ImagesRepository
import com.example.waterfall.unstableinternetconnection.presentationlayer.main.MainPresenter
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit


class NetworkService : Service() {

    companion object {
        const val BROADCAST_ACTION = "com.example.waterfall.networkservice"
        const val PARAM_STATUS = "network_service_status"
        const val SERVER_STATUS_SUCCESS = 1
        const val SERVER_STATUS_ERROR = 2
    }

    private val imagesRepository: ImagesRepository = ImagesRepository()
    private val pictureDao = App.instance.database?.pictureDao()

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        getPictures()
    }

    private fun getPictures() {
        createAndShowForegroundNotification(123)
        Single.fromCallable {
            if (isDisableInternetConnection()) {
                throw RuntimeException("No internet")
            } else {
                imagesRepository.getImages(MainPresenter.CLIENT_ID).execute()
            }
        }.timeout(2000, TimeUnit.MILLISECONDS)
                .retryWhen { exception -> exception.delay(1, TimeUnit.SECONDS) }
                .subscribeOn(Schedulers.io())
                .subscribe({
                    it.body()?.let { pictureDao?.insert(it) }
                    sendBroadcast(SERVER_STATUS_SUCCESS)
                    stopService()
                }, {
                    sendBroadcast(SERVER_STATUS_ERROR)
                    stopService()
                })
    }

    private fun createAndShowForegroundNotification(notificationId: Int) {
        val builder = getNotificationBuilder(this,
                "com.example.waterfall.notification.CHANNEL_ID_FOREGROUND",
                NotificationManagerCompat.IMPORTANCE_LOW)
        builder.setOngoing(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Title")
                .setContentText("Description")

        val notification = builder.build()
        startForeground(notificationId, notification)
    }

    private fun getNotificationBuilder(context: Context, channelId: String, importance: Int): NotificationCompat.Builder =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                prepareChannel(context, channelId, importance)
                NotificationCompat.Builder(context, channelId)
            } else {
                NotificationCompat.Builder(context)
            }


    @TargetApi(26)
    private fun prepareChannel(context: Context, id: String, importance: Int) {
        val appName = context.getString(R.string.app_name)
        val description = "Test"
        val nm = context.getSystemService(Activity.NOTIFICATION_SERVICE) as? NotificationManager

        if (nm != null) {
            var nChannel: NotificationChannel? = nm.getNotificationChannel(id)

            if (nChannel == null) {
                nChannel = NotificationChannel(id, appName, importance)
                nChannel.description = description
                nm.createNotificationChannel(nChannel)
            }
        }
    }

    private fun sendBroadcast(status: Int){
        sendBroadcast(Intent(BROADCAST_ACTION).putExtra(PARAM_STATUS, status))

    }

    private fun isDisableInternetConnection(): Boolean {
        return Math.random() < 0.9
    }

    private fun stopService() {
        stopForeground(true)
        stopSelf()
    }

}