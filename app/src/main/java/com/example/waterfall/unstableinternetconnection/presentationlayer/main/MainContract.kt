package com.example.waterfall.unstableinternetconnection.presentationlayer.main

import com.example.waterfall.unstableinternetconnection.datalayer.models.Picture

interface IMainView {
    fun showError()

    fun showData(list: List<Picture>)

    fun showProgress()

    fun hideProgress()

    fun startNetworkService()
}

interface IMainPresenter {

    fun onAttachView(view: IMainView)

    fun onDetachView()

    fun onBtnTryToConnectClick()

    fun onServerStatusSuccess()

    fun onServerStatusError()

}