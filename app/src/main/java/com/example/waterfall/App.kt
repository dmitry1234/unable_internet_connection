package com.example.waterfall

import com.example.waterfall.unstableinternetconnection.datalayer.database.AppDatabase
import android.arch.persistence.room.Room
import android.app.Application


class App : Application() {

    var database: AppDatabase? = null
        private set

    override fun onCreate() {
        super.onCreate()
        instance = this
        database = Room.databaseBuilder(this, AppDatabase::class.java, "database")
                .build()
    }

    companion object {
        lateinit var instance: App
    }
}
